package kode.kinopoisk.filinski;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.support.v4.content.res.ResourcesCompat;

import kode.kinopoisk.filinski.utils.L;
import kode.kinopoisk.filinski.utils.SpHelper;

/**
 * Created by Fill on 26.09.2016.
 */

public class PreferencesActivity extends PreferenceActivity {

    static SpHelper spHelper;
    static Context context;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();
        spHelper = SpHelper.getInstance(this);
        context = this;
    }


    public static class MyPreferenceFragment extends PreferenceFragment
    {

        Preference cityPreference;

        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref);


            cityPreference = this.getPreferenceScreen().findPreference("city_id");


            setSummary(spHelper.getCity()>0 ? String.valueOf(spHelper.getCity()) : "0");



            cityPreference.setOnPreferenceChangeListener((p, o) -> {
                setSummary(o.toString());
                return true;
            });
        }

        private void setSummary(String newSummary){
            spHelper.setCity(Integer.valueOf(newSummary));

            String[] index = context.getResources().getStringArray(R.array.cityIds);
            String[] values = context.getResources().getStringArray(R.array.cities);

            for(int i=0; i<index.length; i++){
                if(newSummary.equals(index[i])){
                    cityPreference.setSummary(values[i]);
                }
            }
        }
    }
}
