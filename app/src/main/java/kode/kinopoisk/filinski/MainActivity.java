package kode.kinopoisk.filinski;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import kode.kinopoisk.filinski.http.Gateway;
import kode.kinopoisk.filinski.http.pojo.AbstractPojo;
import kode.kinopoisk.filinski.http.pojo.FilmPojo;
import kode.kinopoisk.filinski.http.pojo.Genres;
import kode.kinopoisk.filinski.http.pojo.TodayFilmsPojo;
import kode.kinopoisk.filinski.utils.L;
import kode.kinopoisk.filinski.utils.SpHelper;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class MainActivity extends AppCompatActivity {

    Gateway gateway;

    @BindView(R.id.list)
    RecyclerView list;

    @BindView(R.id.genre_list)
    RecyclerView genre_list;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;

    @BindView(R.id.drawer)
    View drawer;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    SpHelper spHelper;

    String genreId = "0";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        gateway = Gateway.getInstance();

        list.setLayoutManager(new GridLayoutManager(this,2));
        list.setHasFixedSize(true);

        genre_list.setLayoutManager(new LinearLayoutManager(this));
        genre_list.setHasFixedSize(false);


        swipe.setOnRefreshListener(this::todayFilms);

        spHelper = SpHelper.getInstance(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.mipmap.ic_menu_white_24dp));

    }

    @Override
    protected void onResume() {
        super.onResume();
        todayFilms();
        getGenres();
    }


    private void todayFilms(){
        swipe.setRefreshing(true);

        Observable<AbstractPojo> observable;

        if(spHelper.getCity()>0){
            observable = gateway.todayFilms(spHelper.getCity(), genreId);
        }else{
            observable = gateway.todayFilms(1, genreId);
        }
         observable = observable.cache()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
                observable.subscribe(abstractPojo -> {
                    TodayFilmsPojo todayPojo = (TodayFilmsPojo)abstractPojo;
                    if(spHelper.getSort()>0){
                        Observable.from(todayPojo.getFilmsData())
                                .map(filmsDatum -> {
                                    filmsDatum.setRating(filmsDatum.getRating().split(" ")[0]);
                                    return filmsDatum;
                                })
                                .toList()
                                .map(filmsData -> {
                                    Collections.sort(filmsData, (t1, t2) -> {
                                        Float f1 = Float.valueOf(t1.getRating());
                                        Float f2 = Float.valueOf(t2.getRating());
                                        if (f1 < f2) {
                                            return 1;
                                        } else if (f1 > f2) {
                                            return -1;
                                        } else {
                                            return 0;
                                        }
                                    });
                                    return filmsData;
                                })
                                .subscribe(todayPojo::setFilmsData);
                    }
                    FilmsAdapter adapter = new FilmsAdapter(todayPojo, this);
                    list.setAdapter(adapter);
                    swipe.setRefreshing(false);
                });
    }

    private void getGenres(){
        gateway.genres()
                .cache()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(abstractPojo -> {
                    GenreAdapter adapter = new GenreAdapter(((Genres) abstractPojo));
                    genre_list.setAdapter(adapter);
                });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(this).inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.settings:
                Intent intent = new Intent(this, PreferencesActivity.class);
                startActivity(intent);
                break;

            case R.id.by_range:
                if(item.isChecked()){
                    item.setChecked(false);
                    spHelper.setSort(0);
                }else{
                    item.setChecked(true);
                    spHelper.setSort(1);
                }
                todayFilms();

                break;

            case android.R.id.home:
                if (drawer_layout.isDrawerVisible(drawer)) {
                    drawer_layout.closeDrawer(drawer);
                }else {
                    drawer_layout.openDrawer(drawer);
                }

            default: break;
        }
        return super.onOptionsItemSelected(item);
    }




    public class GenreAdapter extends RecyclerView.Adapter<GenreViewHolder>{

        Genres genres;

        GenreAdapter(Genres genres) {
            this.genres = genres;
        }

        @Override
        public GenreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_genre, parent, false);
            return new GenreViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(GenreViewHolder holder, int position) {

            if(position==0){
                holder.genre_tv.setText("Все");
                holder.genre_item.setOnClickListener(v->{
                    genreId = "0";
                    MainActivity.this.todayFilms();
                    if (drawer_layout.isDrawerVisible(drawer)) {
                        drawer_layout.closeDrawer(drawer);
                    }else {
                        drawer_layout.openDrawer(drawer);
                    }
                });
            }else{
                holder.genre_tv.setText(genres.getGenreData().get(position-1).getGenreName());
                holder.genre_item.setOnClickListener(v->{
                    genreId = genres.getGenreData().get(position-1).getGenreID();
                    MainActivity.this.todayFilms();
                    if (drawer_layout.isDrawerVisible(drawer)) {
                        drawer_layout.closeDrawer(drawer);
                    }else {
                        drawer_layout.openDrawer(drawer);
                    }
                });
            }


        }



        @Override
        public int getItemCount() {
            return genres.getGenreData().size()+1;
        }
    }



    public class GenreViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.genre_tv)
        TextView genre_tv;
        @BindView(R.id.genre_item)
        LinearLayout genre_item;

        GenreViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }



    public class FilmsAdapter extends RecyclerView.Adapter<FilmViewHolder>{

        TodayFilmsPojo pojo;
        Context context;

        public FilmsAdapter(TodayFilmsPojo pojo, Context context) {
            this.pojo = pojo;
            this.context = context;
        }

        @Override
        public FilmViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_film, parent, false);
            return new FilmViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(FilmViewHolder holder, int position) {

            holder.text.setText(pojo.getFilmsData().get(position).getNameRU() + "  " + pojo.getFilmsData().get(position).getRating());

            String posterUrl = "https://st.kp.yandex.net/images/"+pojo.getFilmsData().get(position).getPosterURL();
            posterUrl = posterUrl.replace("iphone_", "iphone360_"); // глюк API
            Picasso.with(context).load(posterUrl).into(holder.image);


            String filmId = pojo.getFilmsData().get(position).getId();

            holder.card.setOnClickListener(v->{
                Intent intent = new Intent(MainActivity.this, FilmActivity.class);
                intent.putExtra("filmID", filmId);
                startActivity(intent);
            });

        }

        @Override
        public int getItemCount() {
            return pojo.getFilmsData().size();
        }
    }
    public class FilmViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.image)
        ImageView image;

        @BindView(R.id.text)
        TextView text;

        @BindView(R.id.card)
        CardView card;

        public FilmViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }


}
