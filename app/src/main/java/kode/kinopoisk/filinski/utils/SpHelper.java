package kode.kinopoisk.filinski.utils;

import android.content.Context;
import android.content.SharedPreferences;


public class SpHelper {

    Context context;

    static SpHelper instance;

    SharedPreferences spref;

    static final String FIRST_RUN = "first_run";


    static final String CITY = "city_id";
    static final String SORT = "sort";



    synchronized public static SpHelper getInstance(Context context) {
        if (instance == null) {
            instance = new SpHelper(context);
        }
        return instance;
    }

    private SpHelper(Context context) {
        this.context = context;
        spref = context.getSharedPreferences("my_pref", Context.MODE_APPEND);
    }


    private void setString(String key, String value) {
        spref.edit().putString(key, value).apply();
    }

    private String getString(String key) {
        return spref.getString(key, "");
    }



    private void setInt(String key, int value) {
        spref.edit().putInt(key, value).apply();
    }


    private int getInt(String key) {
        return spref.getInt(key, 0);
    }

    private void setBoolean(String key, boolean value) {
        spref.edit().putBoolean(key, value).apply();
    }

    private boolean getBoolean(String key) {
        return spref.getBoolean(key, false);
    }


    public void setValue(String key, String value) {
        setString(key, value);
    }

    public String getValue(String key) {
        return getString(key);
    }


    public boolean getFirstRun() {
        return !getBoolean(FIRST_RUN);
    }

    public void setFirstRun() {
        setBoolean(FIRST_RUN, false);
    }


    public int getSort(){
        return getInt(SORT);
    }
    public void setSort(int sort){
        setInt(SORT,sort);
    }

    public int getCity(){
        return getInt(CITY);
    }
    public void setCity(int city){
        setInt(CITY,city);
    }

}