package kode.kinopoisk.filinski.http;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import kode.kinopoisk.filinski.http.pojo.AbstractPojo;
import kode.kinopoisk.filinski.http.pojo.ErrorPojo;
import kode.kinopoisk.filinski.utils.L;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

/**
 * Created by Fill on 25.09.2016.
 */

public class Gateway {

    static final int RETRY_COUNT = 3;

    static final String NETWORK_ERROR_MESSSAGE = "Network error";
    static final String RETRIEVE_ERROR_MESAGE = "Exception in response.errorBody()";

    Retrofit retrofit;

    ApiFactory.Kinopoisk kinopoisk;


    private static Gateway instance;

    public synchronized static Gateway getInstance() {
        if (instance == null) {
            instance = new Gateway();
        }
        return instance;
    }

    private Gateway(){

        retrofit = new Retrofit.Builder()
                    .baseUrl(ApiFactory.APIURL)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(
                            new GsonBuilder().create()))
                    .build();


        kinopoisk = retrofit.create(ApiFactory.Kinopoisk.class);

    }


    private Observable<AbstractPojo> verify(Observable<? extends AbstractPojo> observable) {
        return observable
                .cast(AbstractPojo.class)
                .retry(RETRY_COUNT)
                .onErrorReturn(throwable -> {
                    L.trace(throwable.getMessage());

                    if (throwable instanceof HttpException) {
                        HttpException exception = (HttpException) throwable;
                        return generateError(exception.response());
                    } else {
                        ErrorPojo errorPojo = new ErrorPojo();
                        errorPojo.setCode(502);
                        errorPojo.setMessage(NETWORK_ERROR_MESSSAGE);
                        return errorPojo;
                    }
                });
    }

    private ErrorPojo generateError(Response response) {
        ErrorPojo errorPojo;
        try {
            errorPojo = new Gson().fromJson(response.errorBody().string(), ErrorPojo.class);
            errorPojo.setCode(response.code());
            /*if (errorPojo.getCode() == 403) {
                App.getSpHelper().clearApiSession();
                sendAccessDeniedNotify();
            }
            if (errorPojo.getCode() == 410) {
                sendError410Notify();
            }*/
            return errorPojo;
        } catch (IOException e) {
            errorPojo = new ErrorPojo();
            errorPojo.setMessage(RETRIEVE_ERROR_MESAGE);
            errorPojo.setCode(500);
            return errorPojo;
        }
    }


    public Observable<AbstractPojo> todayFilms(long cityID, String genre) {
        return verify(kinopoisk.getTodayFilms(cityID, genre));
    }

    public Observable<AbstractPojo> todayFilms(long cityID){
        return verify(kinopoisk.getTodayFilms(cityID));
    }

    public Observable<AbstractPojo> cityList(int countryID){
        return verify(kinopoisk.getCity(countryID));
    }

    public Observable<AbstractPojo> genres(){
        return verify(kinopoisk.getGenres());
    }

    public Observable<AbstractPojo> film(long filmID){
        return verify(kinopoisk.getFilm(filmID));
    }

    public Observable<AbstractPojo> seance(long filmID, long cityID){
        return verify(kinopoisk.getSeances(filmID, cityID));
    }



}
