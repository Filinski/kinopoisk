package kode.kinopoisk.filinski.http.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fill on 25.09.2016.
 */

public class Genres extends AbstractPojo {

        @SerializedName("genreData")
        @Expose
        private List<GenreDatum> genreData = new ArrayList<GenreDatum>();

        /**
         *
         * @return
         * The genreData
         */
        public List<GenreDatum> getGenreData() {
            return genreData;
        }

        /**
         *
         * @param genreData
         * The genreData
         */
        public void setGenreData(List<GenreDatum> genreData) {
            this.genreData = genreData;
        }


    public class GenreDatum implements Serializable{

        @SerializedName("genreID")
        @Expose
        private String genreID;
        @SerializedName("genreName")
        @Expose
        private String genreName;

        /**
         *
         * @return
         * The genreID
         */
        public String getGenreID() {
            return genreID;
        }

        /**
         *
         * @param genreID
         * The genreID
         */
        public void setGenreID(String genreID) {
            this.genreID = genreID;
        }

        /**
         *
         * @return
         * The genreName
         */
        public String getGenreName() {
            return genreName;
        }

        /**
         *
         * @param genreName
         * The genreName
         */
        public void setGenreName(String genreName) {
            this.genreName = genreName;
        }

    }


}
