package kode.kinopoisk.filinski.http.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fill on 25.09.2016.
 */


public class TodayFilmsPojo  extends AbstractPojo{

    @SerializedName("filmsData")
    @Expose
    private List<FilmsDatum> filmsData = new ArrayList<FilmsDatum>();
    @SerializedName("date")
    @Expose
    private String date;

    /**
     *
     * @return
     * The filmsData
     */
    public List<FilmsDatum> getFilmsData() {
        return filmsData;
    }

    /**
     *
     * @param filmsData
     * The filmsData
     */
    public void setFilmsData(List<FilmsDatum> filmsData) {
        this.filmsData = filmsData;
    }

    /**
     *
     * @return
     * The date
     */
    public String getDate() {
        return date;
    }

    /**
     *
     * @param date
     * The date
     */
    public void setDate(String date) {
        this.date = date;
    }




    public static class FilmsDatum implements Serializable{

        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("nameRU")
        @Expose
        private String nameRU;
        @SerializedName("nameEN")
        @Expose
        private String nameEN;
        @SerializedName("year")
        @Expose
        private String year;
        @SerializedName("cinemaHallCount")
        @Expose
        private String cinemaHallCount;
        @SerializedName("is3D")
        @Expose
        private Integer is3D;
        @SerializedName("isNew")
        @Expose
        private Integer isNew;
        @SerializedName("rating")
        @Expose
        private String rating;
        @SerializedName("posterURL")
        @Expose
        private String posterURL;
        @SerializedName("filmLength")
        @Expose
        private String filmLength;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("genre")
        @Expose
        private String genre;
        @SerializedName("premiereRU")
        @Expose
        private String premiereRU;
        @SerializedName("videoURL")
        @Expose
        private VideoURL videoURL;
        @SerializedName("isIMAX")
        @Expose
        private Integer isIMAX;

        /**
         *
         * @return
         * The type
         */
        public String getType() {
            return type;
        }

        /**
         *
         * @param type
         * The type
         */
        public void setType(String type) {
            this.type = type;
        }

        /**
         *
         * @return
         * The id
         */
        public String getId() {
            return id;
        }

        /**
         *
         * @param id
         * The id
         */
        public void setId(String id) {
            this.id = id;
        }

        /**
         *
         * @return
         * The nameRU
         */
        public String getNameRU() {
            return nameRU;
        }

        /**
         *
         * @param nameRU
         * The nameRU
         */
        public void setNameRU(String nameRU) {
            this.nameRU = nameRU;
        }

        /**
         *
         * @return
         * The nameEN
         */
        public String getNameEN() {
            return nameEN;
        }

        /**
         *
         * @param nameEN
         * The nameEN
         */
        public void setNameEN(String nameEN) {
            this.nameEN = nameEN;
        }

        /**
         *
         * @return
         * The year
         */
        public String getYear() {
            return year;
        }

        /**
         *
         * @param year
         * The year
         */
        public void setYear(String year) {
            this.year = year;
        }

        /**
         *
         * @return
         * The cinemaHallCount
         */
        public String getCinemaHallCount() {
            return cinemaHallCount;
        }

        /**
         *
         * @param cinemaHallCount
         * The cinemaHallCount
         */
        public void setCinemaHallCount(String cinemaHallCount) {
            this.cinemaHallCount = cinemaHallCount;
        }

        /**
         *
         * @return
         * The is3D
         */
        public Integer getIs3D() {
            return is3D;
        }

        /**
         *
         * @param is3D
         * The is3D
         */
        public void setIs3D(Integer is3D) {
            this.is3D = is3D;
        }

        /**
         *
         * @return
         * The isNew
         */
        public Integer getIsNew() {
            return isNew;
        }

        /**
         *
         * @param isNew
         * The isNew
         */
        public void setIsNew(Integer isNew) {
            this.isNew = isNew;
        }

        /**
         *
         * @return
         * The rating
         */
        public String getRating() {
            return rating;
        }

        /**
         *
         * @param rating
         * The rating
         */
        public void setRating(String rating) {
            this.rating = rating;
        }

        /**
         *
         * @return
         * The posterURL
         */
        public String getPosterURL() {
            return posterURL;
        }

        /**
         *
         * @param posterURL
         * The posterURL
         */
        public void setPosterURL(String posterURL) {
            this.posterURL = posterURL;
        }

        /**
         *
         * @return
         * The filmLength
         */
        public String getFilmLength() {
            return filmLength;
        }

        /**
         *
         * @param filmLength
         * The filmLength
         */
        public void setFilmLength(String filmLength) {
            this.filmLength = filmLength;
        }

        /**
         *
         * @return
         * The country
         */
        public String getCountry() {
            return country;
        }

        /**
         *
         * @param country
         * The country
         */
        public void setCountry(String country) {
            this.country = country;
        }

        /**
         *
         * @return
         * The genre
         */
        public String getGenre() {
            return genre;
        }

        /**
         *
         * @param genre
         * The genre
         */
        public void setGenre(String genre) {
            this.genre = genre;
        }

        /**
         *
         * @return
         * The premiereRU
         */
        public String getPremiereRU() {
            return premiereRU;
        }

        /**
         *
         * @param premiereRU
         * The premiereRU
         */
        public void setPremiereRU(String premiereRU) {
            this.premiereRU = premiereRU;
        }

        /**
         *
         * @return
         * The videoURL
         */
        public VideoURL getVideoURL() {
            return videoURL;
        }

        /**
         *
         * @param videoURL
         * The videoURL
         */
        public void setVideoURL(VideoURL videoURL) {
            this.videoURL = videoURL;
        }

        /**
         *
         * @return
         * The isIMAX
         */
        public Integer getIsIMAX() {
            return isIMAX;
        }

        /**
         *
         * @param isIMAX
         * The isIMAX
         */
        public void setIsIMAX(Integer isIMAX) {
            this.isIMAX = isIMAX;
        }

    }



    public static class VideoURL implements Serializable{

        @SerializedName("hd")
        @Expose
        private String hd;
        @SerializedName("sd")
        @Expose
        private String sd;
        @SerializedName("low")
        @Expose
        private String low;

        /**
         *
         * @return
         * The hd
         */
        public String getHd() {
            return hd;
        }

        /**
         *
         * @param hd
         * The hd
         */
        public void setHd(String hd) {
            this.hd = hd;
        }

        /**
         *
         * @return
         * The sd
         */
        public String getSd() {
            return sd;
        }

        /**
         *
         * @param sd
         * The sd
         */
        public void setSd(String sd) {
            this.sd = sd;
        }

        /**
         *
         * @return
         * The low
         */
        public String getLow() {
            return low;
        }

        /**
         *
         * @param low
         * The low
         */
        public void setLow(String low) {
            this.low = low;
        }

    }

}
