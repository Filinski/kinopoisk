package kode.kinopoisk.filinski.http.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fill on 25.09.2016.
 */

public class CityPojo extends AbstractPojo {

        @SerializedName("countryID")
        @Expose
        private String countryID;
        @SerializedName("countryName")
        @Expose
        private String countryName;
        @SerializedName("cityData")
        @Expose
        private List<CityDatum> cityData = new ArrayList<CityDatum>();

        /**
         *
         * @return
         * The countryID
         */
        public String getCountryID() {
            return countryID;
        }

        /**
         *
         * @param countryID
         * The countryID
         */
        public void setCountryID(String countryID) {
            this.countryID = countryID;
        }

        /**
         *
         * @return
         * The countryName
         */
        public String getCountryName() {
            return countryName;
        }

        /**
         *
         * @param countryName
         * The countryName
         */
        public void setCountryName(String countryName) {
            this.countryName = countryName;
        }

        /**
         *
         * @return
         * The cityData
         */
        public List<CityDatum> getCityData() {
            return cityData;
        }

        /**
         *
         * @param cityData
         * The cityData
         */
        public void setCityData(List<CityDatum> cityData) {
            this.cityData = cityData;
        }


    public class CityDatum implements Serializable{

        @SerializedName("cityID")
        @Expose
        private String cityID;
        @SerializedName("cityName")
        @Expose
        private String cityName;

        /**
         *
         * @return
         * The cityID
         */
        public String getCityID() {
            return cityID;
        }

        /**
         *
         * @param cityID
         * The cityID
         */
        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        /**
         *
         * @return
         * The cityName
         */
        public String getCityName() {
            return cityName;
        }

        /**
         *
         * @param cityName
         * The cityName
         */
        public void setCityName(String cityName) {
            this.cityName = cityName;
        }

    }


}
