package kode.kinopoisk.filinski.http.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fill on 25.09.2016.
 */

public class FilmPojo extends AbstractPojo {

        @SerializedName("hasSimilarFilms")
        @Expose
        private Integer hasSimilarFilms;
        @SerializedName("reviewsCount")
        @Expose
        private Integer reviewsCount;
        @SerializedName("ratingData")
        @Expose
        private RatingData ratingData;
        @SerializedName("hasSequelsAndPrequelsFilms")
        @Expose
        private Integer hasSequelsAndPrequelsFilms;
        @SerializedName("hasRelatedFilms")
        @Expose
        private Integer hasRelatedFilms;
        @SerializedName("filmID")
        @Expose
        private String filmID;
        @SerializedName("webURL")
        @Expose
        private String webURL;
        @SerializedName("nameRU")
        @Expose
        private String nameRU;
        @SerializedName("nameEN")
        @Expose
        private String nameEN;
        @SerializedName("posterURL")
        @Expose
        private String posterURL;
        @SerializedName("year")
        @Expose
        private String year;
        @SerializedName("filmLength")
        @Expose
        private String filmLength;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("genre")
        @Expose
        private String genre;
        @SerializedName("slogan")
        @Expose
        private String slogan;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("videoURL")
        @Expose
        private VideoURL videoURL;
        @SerializedName("ratingMPAA")
        @Expose
        private String ratingMPAA;
        @SerializedName("isIMAX")
        @Expose
        private Integer isIMAX;
        @SerializedName("is3D")
        @Expose
        private Integer is3D;
        @SerializedName("ratingAgeLimits")
        @Expose
        private String ratingAgeLimits;
        @SerializedName("rentData")
        @Expose
        private RentData rentData;
        @SerializedName("budgetData")
        @Expose
        private BudgetData budgetData;
        @SerializedName("gallery")
        @Expose
        private List<Gallery> gallery = new ArrayList<Gallery>();
        @SerializedName("creators")
        @Expose
        private List<List<Creator>> creators = new ArrayList<List<Creator>>();
        @SerializedName("topNewsByFilm")
        @Expose
        private TopNewsByFilm topNewsByFilm;
        @SerializedName("triviaData")
        @Expose
        private List<String> triviaData = new ArrayList<String>();
        @SerializedName("imdbID")
        @Expose
        private String imdbID;

        /**
         *
         * @return
         * The hasSimilarFilms
         */
        public Integer getHasSimilarFilms() {
            return hasSimilarFilms;
        }

        /**
         *
         * @param hasSimilarFilms
         * The hasSimilarFilms
         */
        public void setHasSimilarFilms(Integer hasSimilarFilms) {
            this.hasSimilarFilms = hasSimilarFilms;
        }

        /**
         *
         * @return
         * The reviewsCount
         */
        public Integer getReviewsCount() {
            return reviewsCount;
        }

        /**
         *
         * @param reviewsCount
         * The reviewsCount
         */
        public void setReviewsCount(Integer reviewsCount) {
            this.reviewsCount = reviewsCount;
        }

        /**
         *
         * @return
         * The ratingData
         */
        public RatingData getRatingData() {
            return ratingData;
        }

        /**
         *
         * @param ratingData
         * The ratingData
         */
        public void setRatingData(RatingData ratingData) {
            this.ratingData = ratingData;
        }

        /**
         *
         * @return
         * The hasSequelsAndPrequelsFilms
         */
        public Integer getHasSequelsAndPrequelsFilms() {
            return hasSequelsAndPrequelsFilms;
        }

        /**
         *
         * @param hasSequelsAndPrequelsFilms
         * The hasSequelsAndPrequelsFilms
         */
        public void setHasSequelsAndPrequelsFilms(Integer hasSequelsAndPrequelsFilms) {
            this.hasSequelsAndPrequelsFilms = hasSequelsAndPrequelsFilms;
        }

        /**
         *
         * @return
         * The hasRelatedFilms
         */
        public Integer getHasRelatedFilms() {
            return hasRelatedFilms;
        }

        /**
         *
         * @param hasRelatedFilms
         * The hasRelatedFilms
         */
        public void setHasRelatedFilms(Integer hasRelatedFilms) {
            this.hasRelatedFilms = hasRelatedFilms;
        }

        /**
         *
         * @return
         * The filmID
         */
        public String getFilmID() {
            return filmID;
        }

        /**
         *
         * @param filmID
         * The filmID
         */
        public void setFilmID(String filmID) {
            this.filmID = filmID;
        }

        /**
         *
         * @return
         * The webURL
         */
        public String getWebURL() {
            return webURL;
        }

        /**
         *
         * @param webURL
         * The webURL
         */
        public void setWebURL(String webURL) {
            this.webURL = webURL;
        }

        /**
         *
         * @return
         * The nameRU
         */
        public String getNameRU() {
            return nameRU;
        }

        /**
         *
         * @param nameRU
         * The nameRU
         */
        public void setNameRU(String nameRU) {
            this.nameRU = nameRU;
        }

        /**
         *
         * @return
         * The nameEN
         */
        public String getNameEN() {
            return nameEN;
        }

        /**
         *
         * @param nameEN
         * The nameEN
         */
        public void setNameEN(String nameEN) {
            this.nameEN = nameEN;
        }

        /**
         *
         * @return
         * The posterURL
         */
        public String getPosterURL() {
            return posterURL;
        }

        /**
         *
         * @param posterURL
         * The posterURL
         */
        public void setPosterURL(String posterURL) {
            this.posterURL = posterURL;
        }

        /**
         *
         * @return
         * The year
         */
        public String getYear() {
            return year;
        }

        /**
         *
         * @param year
         * The year
         */
        public void setYear(String year) {
            this.year = year;
        }

        /**
         *
         * @return
         * The filmLength
         */
        public String getFilmLength() {
            return filmLength;
        }

        /**
         *
         * @param filmLength
         * The filmLength
         */
        public void setFilmLength(String filmLength) {
            this.filmLength = filmLength;
        }

        /**
         *
         * @return
         * The country
         */
        public String getCountry() {
            return country;
        }

        /**
         *
         * @param country
         * The country
         */
        public void setCountry(String country) {
            this.country = country;
        }

        /**
         *
         * @return
         * The genre
         */
        public String getGenre() {
            return genre;
        }

        /**
         *
         * @param genre
         * The genre
         */
        public void setGenre(String genre) {
            this.genre = genre;
        }

        /**
         *
         * @return
         * The slogan
         */
        public String getSlogan() {
            return slogan;
        }

        /**
         *
         * @param slogan
         * The slogan
         */
        public void setSlogan(String slogan) {
            this.slogan = slogan;
        }

        /**
         *
         * @return
         * The description
         */
        public String getDescription() {
            return description;
        }

        /**
         *
         * @param description
         * The description
         */
        public void setDescription(String description) {
            this.description = description;
        }

        /**
         *
         * @return
         * The videoURL
         */
        public VideoURL getVideoURL() {
            return videoURL;
        }

        /**
         *
         * @param videoURL
         * The videoURL
         */
        public void setVideoURL(VideoURL videoURL) {
            this.videoURL = videoURL;
        }

        /**
         *
         * @return
         * The ratingMPAA
         */
        public String getRatingMPAA() {
            return ratingMPAA;
        }

        /**
         *
         * @param ratingMPAA
         * The ratingMPAA
         */
        public void setRatingMPAA(String ratingMPAA) {
            this.ratingMPAA = ratingMPAA;
        }

        /**
         *
         * @return
         * The isIMAX
         */
        public Integer getIsIMAX() {
            return isIMAX;
        }

        /**
         *
         * @param isIMAX
         * The isIMAX
         */
        public void setIsIMAX(Integer isIMAX) {
            this.isIMAX = isIMAX;
        }

        /**
         *
         * @return
         * The is3D
         */
        public Integer getIs3D() {
            return is3D;
        }

        /**
         *
         * @param is3D
         * The is3D
         */
        public void setIs3D(Integer is3D) {
            this.is3D = is3D;
        }

        /**
         *
         * @return
         * The ratingAgeLimits
         */
        public String getRatingAgeLimits() {
            return ratingAgeLimits;
        }

        /**
         *
         * @param ratingAgeLimits
         * The ratingAgeLimits
         */
        public void setRatingAgeLimits(String ratingAgeLimits) {
            this.ratingAgeLimits = ratingAgeLimits;
        }

        /**
         *
         * @return
         * The rentData
         */
        public RentData getRentData() {
            return rentData;
        }

        /**
         *
         * @param rentData
         * The rentData
         */
        public void setRentData(RentData rentData) {
            this.rentData = rentData;
        }

        /**
         *
         * @return
         * The budgetData
         */
        public BudgetData getBudgetData() {
            return budgetData;
        }

        /**
         *
         * @param budgetData
         * The budgetData
         */
        public void setBudgetData(BudgetData budgetData) {
            this.budgetData = budgetData;
        }

        /**
         *
         * @return
         * The gallery
         */
        public List<Gallery> getGallery() {
            return gallery;
        }

        /**
         *
         * @param gallery
         * The gallery
         */
        public void setGallery(List<Gallery> gallery) {
            this.gallery = gallery;
        }

        /**
         *
         * @return
         * The creators
         */
        public List<List<Creator>> getCreators() {
            return creators;
        }

        /**
         *
         * @param creators
         * The creators
         */
        public void setCreators(List<List<Creator>> creators) {
            this.creators = creators;
        }

        /**
         *
         * @return
         * The topNewsByFilm
         */
        public TopNewsByFilm getTopNewsByFilm() {
            return topNewsByFilm;
        }

        /**
         *
         * @param topNewsByFilm
         * The topNewsByFilm
         */
        public void setTopNewsByFilm(TopNewsByFilm topNewsByFilm) {
            this.topNewsByFilm = topNewsByFilm;
        }

        /**
         *
         * @return
         * The triviaData
         */
        public List<String> getTriviaData() {
            return triviaData;
        }

        /**
         *
         * @param triviaData
         * The triviaData
         */
        public void setTriviaData(List<String> triviaData) {
            this.triviaData = triviaData;
        }

        /**
         *
         * @return
         * The imdbID
         */
        public String getImdbID() {
            return imdbID;
        }

        /**
         *
         * @param imdbID
         * The imdbID
         */
        public void setImdbID(String imdbID) {
            this.imdbID = imdbID;
        }




    public static class Gallery implements Serializable{

        @SerializedName("preview")
        @Expose
        private String preview;

        /**
         *
         * @return
         * The preview
         */
        public String getPreview() {
            return preview;
        }

        /**
         *
         * @param preview
         * The preview
         */
        public void setPreview(String preview) {
            this.preview = preview;
        }

    }


    public static class RatingData implements Serializable {

        @SerializedName("ratingGoodReview")
        @Expose
        private String ratingGoodReview;
        @SerializedName("ratingGoodReviewVoteCount")
        @Expose
        private Integer ratingGoodReviewVoteCount;
        @SerializedName("rating")
        @Expose
        private String rating;
        @SerializedName("ratingVoteCount")
        @Expose
        private String ratingVoteCount;
        @SerializedName("ratingAwait")
        @Expose
        private String ratingAwait;
        @SerializedName("ratingAwaitCount")
        @Expose
        private String ratingAwaitCount;
        @SerializedName("ratingIMDb")
        @Expose
        private String ratingIMDb;
        @SerializedName("ratingIMDbVoteCount")
        @Expose
        private String ratingIMDbVoteCount;
        @SerializedName("ratingFilmCritics")
        @Expose
        private String ratingFilmCritics;
        @SerializedName("ratingFilmCriticsVoteCount")
        @Expose
        private String ratingFilmCriticsVoteCount;
        @SerializedName("ratingRFCritics")
        @Expose
        private String ratingRFCritics;
        @SerializedName("ratingRFCriticsVoteCount")
        @Expose
        private Integer ratingRFCriticsVoteCount;

        /**
         *
         * @return
         * The ratingGoodReview
         */
        public String getRatingGoodReview() {
            return ratingGoodReview;
        }

        /**
         *
         * @param ratingGoodReview
         * The ratingGoodReview
         */
        public void setRatingGoodReview(String ratingGoodReview) {
            this.ratingGoodReview = ratingGoodReview;
        }

        /**
         *
         * @return
         * The ratingGoodReviewVoteCount
         */
        public Integer getRatingGoodReviewVoteCount() {
            return ratingGoodReviewVoteCount;
        }

        /**
         *
         * @param ratingGoodReviewVoteCount
         * The ratingGoodReviewVoteCount
         */
        public void setRatingGoodReviewVoteCount(Integer ratingGoodReviewVoteCount) {
            this.ratingGoodReviewVoteCount = ratingGoodReviewVoteCount;
        }

        /**
         *
         * @return
         * The rating
         */
        public String getRating() {
            return rating;
        }

        /**
         *
         * @param rating
         * The rating
         */
        public void setRating(String rating) {
            this.rating = rating;
        }

        /**
         *
         * @return
         * The ratingVoteCount
         */
        public String getRatingVoteCount() {
            return ratingVoteCount;
        }

        /**
         *
         * @param ratingVoteCount
         * The ratingVoteCount
         */
        public void setRatingVoteCount(String ratingVoteCount) {
            this.ratingVoteCount = ratingVoteCount;
        }

        /**
         *
         * @return
         * The ratingAwait
         */
        public String getRatingAwait() {
            return ratingAwait;
        }

        /**
         *
         * @param ratingAwait
         * The ratingAwait
         */
        public void setRatingAwait(String ratingAwait) {
            this.ratingAwait = ratingAwait;
        }

        /**
         *
         * @return
         * The ratingAwaitCount
         */
        public String getRatingAwaitCount() {
            return ratingAwaitCount;
        }

        /**
         *
         * @param ratingAwaitCount
         * The ratingAwaitCount
         */
        public void setRatingAwaitCount(String ratingAwaitCount) {
            this.ratingAwaitCount = ratingAwaitCount;
        }

        /**
         *
         * @return
         * The ratingIMDb
         */
        public String getRatingIMDb() {
            return ratingIMDb;
        }

        /**
         *
         * @param ratingIMDb
         * The ratingIMDb
         */
        public void setRatingIMDb(String ratingIMDb) {
            this.ratingIMDb = ratingIMDb;
        }

        /**
         *
         * @return
         * The ratingIMDbVoteCount
         */
        public String getRatingIMDbVoteCount() {
            return ratingIMDbVoteCount;
        }

        /**
         *
         * @param ratingIMDbVoteCount
         * The ratingIMDbVoteCount
         */
        public void setRatingIMDbVoteCount(String ratingIMDbVoteCount) {
            this.ratingIMDbVoteCount = ratingIMDbVoteCount;
        }

        /**
         *
         * @return
         * The ratingFilmCritics
         */
        public String getRatingFilmCritics() {
            return ratingFilmCritics;
        }

        /**
         *
         * @param ratingFilmCritics
         * The ratingFilmCritics
         */
        public void setRatingFilmCritics(String ratingFilmCritics) {
            this.ratingFilmCritics = ratingFilmCritics;
        }

        /**
         *
         * @return
         * The ratingFilmCriticsVoteCount
         */
        public String getRatingFilmCriticsVoteCount() {
            return ratingFilmCriticsVoteCount;
        }

        /**
         *
         * @param ratingFilmCriticsVoteCount
         * The ratingFilmCriticsVoteCount
         */
        public void setRatingFilmCriticsVoteCount(String ratingFilmCriticsVoteCount) {
            this.ratingFilmCriticsVoteCount = ratingFilmCriticsVoteCount;
        }

        /**
         *
         * @return
         * The ratingRFCritics
         */
        public String getRatingRFCritics() {
            return ratingRFCritics;
        }

        /**
         *
         * @param ratingRFCritics
         * The ratingRFCritics
         */
        public void setRatingRFCritics(String ratingRFCritics) {
            this.ratingRFCritics = ratingRFCritics;
        }

        /**
         *
         * @return
         * The ratingRFCriticsVoteCount
         */
        public Integer getRatingRFCriticsVoteCount() {
            return ratingRFCriticsVoteCount;
        }

        /**
         *
         * @param ratingRFCriticsVoteCount
         * The ratingRFCriticsVoteCount
         */
        public void setRatingRFCriticsVoteCount(Integer ratingRFCriticsVoteCount) {
            this.ratingRFCriticsVoteCount = ratingRFCriticsVoteCount;
        }

    }




    public static class RentData implements Serializable {

        @SerializedName("premiereRU")
        @Expose
        private String premiereRU;
        @SerializedName("Distributors")
        @Expose
        private String distributors;
        @SerializedName("premiereWorld")
        @Expose
        private String premiereWorld;
        @SerializedName("premiereWorldCountry")
        @Expose
        private String premiereWorldCountry;
        @SerializedName("premiereDVD")
        @Expose
        private String premiereDVD;
        @SerializedName("premiereBluRay")
        @Expose
        private String premiereBluRay;
        @SerializedName("distributorRelease")
        @Expose
        private String distributorRelease;

        /**
         *
         * @return
         * The premiereRU
         */
        public String getPremiereRU() {
            return premiereRU;
        }

        /**
         *
         * @param premiereRU
         * The premiereRU
         */
        public void setPremiereRU(String premiereRU) {
            this.premiereRU = premiereRU;
        }

        /**
         *
         * @return
         * The distributors
         */
        public String getDistributors() {
            return distributors;
        }

        /**
         *
         * @param distributors
         * The Distributors
         */
        public void setDistributors(String distributors) {
            this.distributors = distributors;
        }

        /**
         *
         * @return
         * The premiereWorld
         */
        public String getPremiereWorld() {
            return premiereWorld;
        }

        /**
         *
         * @param premiereWorld
         * The premiereWorld
         */
        public void setPremiereWorld(String premiereWorld) {
            this.premiereWorld = premiereWorld;
        }

        /**
         *
         * @return
         * The premiereWorldCountry
         */
        public String getPremiereWorldCountry() {
            return premiereWorldCountry;
        }

        /**
         *
         * @param premiereWorldCountry
         * The premiereWorldCountry
         */
        public void setPremiereWorldCountry(String premiereWorldCountry) {
            this.premiereWorldCountry = premiereWorldCountry;
        }

        /**
         *
         * @return
         * The premiereDVD
         */
        public String getPremiereDVD() {
            return premiereDVD;
        }

        /**
         *
         * @param premiereDVD
         * The premiereDVD
         */
        public void setPremiereDVD(String premiereDVD) {
            this.premiereDVD = premiereDVD;
        }

        /**
         *
         * @return
         * The premiereBluRay
         */
        public String getPremiereBluRay() {
            return premiereBluRay;
        }

        /**
         *
         * @param premiereBluRay
         * The premiereBluRay
         */
        public void setPremiereBluRay(String premiereBluRay) {
            this.premiereBluRay = premiereBluRay;
        }

        /**
         *
         * @return
         * The distributorRelease
         */
        public String getDistributorRelease() {
            return distributorRelease;
        }

        /**
         *
         * @param distributorRelease
         * The distributorRelease
         */
        public void setDistributorRelease(String distributorRelease) {
            this.distributorRelease = distributorRelease;
        }

    }



    public static class TopNewsByFilm implements Serializable{

        @SerializedName("ID")
        @Expose
        private String iD;
        @SerializedName("newsDate")
        @Expose
        private String newsDate;
        @SerializedName("newsImagePreviewURL")
        @Expose
        private String newsImagePreviewURL;
        @SerializedName("newsTitle")
        @Expose
        private String newsTitle;
        @SerializedName("newsDescription")
        @Expose
        private String newsDescription;

        /**
         *
         * @return
         * The iD
         */
        public String getID() {
            return iD;
        }

        /**
         *
         * @param iD
         * The ID
         */
        public void setID(String iD) {
            this.iD = iD;
        }

        /**
         *
         * @return
         * The newsDate
         */
        public String getNewsDate() {
            return newsDate;
        }

        /**
         *
         * @param newsDate
         * The newsDate
         */
        public void setNewsDate(String newsDate) {
            this.newsDate = newsDate;
        }

        /**
         *
         * @return
         * The newsImagePreviewURL
         */
        public String getNewsImagePreviewURL() {
            return newsImagePreviewURL;
        }

        /**
         *
         * @param newsImagePreviewURL
         * The newsImagePreviewURL
         */
        public void setNewsImagePreviewURL(String newsImagePreviewURL) {
            this.newsImagePreviewURL = newsImagePreviewURL;
        }

        /**
         *
         * @return
         * The newsTitle
         */
        public String getNewsTitle() {
            return newsTitle;
        }

        /**
         *
         * @param newsTitle
         * The newsTitle
         */
        public void setNewsTitle(String newsTitle) {
            this.newsTitle = newsTitle;
        }

        /**
         *
         * @return
         * The newsDescription
         */
        public String getNewsDescription() {
            return newsDescription;
        }

        /**
         *
         * @param newsDescription
         * The newsDescription
         */
        public void setNewsDescription(String newsDescription) {
            this.newsDescription = newsDescription;
        }

    }




    public static class VideoURL  implements Serializable{

        @SerializedName("hd")
        @Expose
        private String hd;
        @SerializedName("sd")
        @Expose
        private String sd;
        @SerializedName("low")
        @Expose
        private String low;

        /**
         *
         * @return
         * The hd
         */
        public String getHd() {
            return hd;
        }

        /**
         *
         * @param hd
         * The hd
         */
        public void setHd(String hd) {
            this.hd = hd;
        }

        /**
         *
         * @return
         * The sd
         */
        public String getSd() {
            return sd;
        }

        /**
         *
         * @param sd
         * The sd
         */
        public void setSd(String sd) {
            this.sd = sd;
        }

        /**
         *
         * @return
         * The low
         */
        public String getLow() {
            return low;
        }

        /**
         *
         * @param low
         * The low
         */
        public void setLow(String low) {
            this.low = low;
        }

    }




    public static class BudgetData implements Serializable{

        @SerializedName("grossRU")
        @Expose
        private String grossRU;
        @SerializedName("grossUSA")
        @Expose
        private String grossUSA;
        @SerializedName("grossWorld")
        @Expose
        private String grossWorld;
        @SerializedName("budget")
        @Expose
        private String budget;

        /**
         *
         * @return
         * The grossRU
         */
        public String getGrossRU() {
            return grossRU;
        }

        /**
         *
         * @param grossRU
         * The grossRU
         */
        public void setGrossRU(String grossRU) {
            this.grossRU = grossRU;
        }

        /**
         *
         * @return
         * The grossUSA
         */
        public String getGrossUSA() {
            return grossUSA;
        }

        /**
         *
         * @param grossUSA
         * The grossUSA
         */
        public void setGrossUSA(String grossUSA) {
            this.grossUSA = grossUSA;
        }

        /**
         *
         * @return
         * The grossWorld
         */
        public String getGrossWorld() {
            return grossWorld;
        }

        /**
         *
         * @param grossWorld
         * The grossWorld
         */
        public void setGrossWorld(String grossWorld) {
            this.grossWorld = grossWorld;
        }

        /**
         *
         * @return
         * The budget
         */
        public String getBudget() {
            return budget;
        }

        /**
         *
         * @param budget
         * The budget
         */
        public void setBudget(String budget) {
            this.budget = budget;
        }

    }



    public static class Creator implements Serializable{

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("nameRU")
        @Expose
        private String nameRU;
        @SerializedName("nameEN")
        @Expose
        private String nameEN;
        @SerializedName("posterURL")
        @Expose
        private String posterURL;
        @SerializedName("professionText")
        @Expose
        private String professionText;
        @SerializedName("professionKey")
        @Expose
        private String professionKey;

        /**
         *
         * @return
         * The id
         */
        public String getId() {
            return id;
        }

        /**
         *
         * @param id
         * The id
         */
        public void setId(String id) {
            this.id = id;
        }

        /**
         *
         * @return
         * The type
         */
        public String getType() {
            return type;
        }

        /**
         *
         * @param type
         * The type
         */
        public void setType(String type) {
            this.type = type;
        }

        /**
         *
         * @return
         * The nameRU
         */
        public String getNameRU() {
            return nameRU;
        }

        /**
         *
         * @param nameRU
         * The nameRU
         */
        public void setNameRU(String nameRU) {
            this.nameRU = nameRU;
        }

        /**
         *
         * @return
         * The nameEN
         */
        public String getNameEN() {
            return nameEN;
        }

        /**
         *
         * @param nameEN
         * The nameEN
         */
        public void setNameEN(String nameEN) {
            this.nameEN = nameEN;
        }

        /**
         *
         * @return
         * The posterURL
         */
        public String getPosterURL() {
            return posterURL;
        }

        /**
         *
         * @param posterURL
         * The posterURL
         */
        public void setPosterURL(String posterURL) {
            this.posterURL = posterURL;
        }

        /**
         *
         * @return
         * The professionText
         */
        public String getProfessionText() {
            return professionText;
        }

        /**
         *
         * @param professionText
         * The professionText
         */
        public void setProfessionText(String professionText) {
            this.professionText = professionText;
        }

        /**
         *
         * @return
         * The professionKey
         */
        public String getProfessionKey() {
            return professionKey;
        }

        /**
         *
         * @param professionKey
         * The professionKey
         */
        public void setProfessionKey(String professionKey) {
            this.professionKey = professionKey;
        }

    }



}
