package kode.kinopoisk.filinski.http.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fill on 25.09.2016.
 */


public class SeancesPojo extends AbstractPojo{

    @SerializedName("cityID")
    @Expose
    private String cityID;
    @SerializedName("cityName")
    @Expose
    private String cityName;
    @SerializedName("seanceURL")
    @Expose
    private String seanceURL;
    @SerializedName("filmID")
    @Expose
    private String filmID;
    @SerializedName("nameRU")
    @Expose
    private String nameRU;
    @SerializedName("nameEN")
    @Expose
    private String nameEN;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("is3D")
    @Expose
    private Integer is3D;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("posterURL")
    @Expose
    private String posterURL;
    @SerializedName("filmLength")
    @Expose
    private String filmLength;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("genre")
    @Expose
    private String genre;
    @SerializedName("videoURL")
    @Expose
    private VideoURL videoURL;
    @SerializedName("items")
    @Expose
    private List<Item> items = new ArrayList<Item>();
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("imdbID")
    @Expose
    private String imdbID;

    /**
     *
     * @return
     * The cityID
     */
    public String getCityID() {
        return cityID;
    }

    /**
     *
     * @param cityID
     * The cityID
     */
    public void setCityID(String cityID) {
        this.cityID = cityID;
    }

    /**
     *
     * @return
     * The cityName
     */
    public String getCityName() {
        return cityName;
    }

    /**
     *
     * @param cityName
     * The cityName
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    /**
     *
     * @return
     * The seanceURL
     */
    public String getSeanceURL() {
        return seanceURL;
    }

    /**
     *
     * @param seanceURL
     * The seanceURL
     */
    public void setSeanceURL(String seanceURL) {
        this.seanceURL = seanceURL;
    }

    /**
     *
     * @return
     * The filmID
     */
    public String getFilmID() {
        return filmID;
    }

    /**
     *
     * @param filmID
     * The filmID
     */
    public void setFilmID(String filmID) {
        this.filmID = filmID;
    }

    /**
     *
     * @return
     * The nameRU
     */
    public String getNameRU() {
        return nameRU;
    }

    /**
     *
     * @param nameRU
     * The nameRU
     */
    public void setNameRU(String nameRU) {
        this.nameRU = nameRU;
    }

    /**
     *
     * @return
     * The nameEN
     */
    public String getNameEN() {
        return nameEN;
    }

    /**
     *
     * @param nameEN
     * The nameEN
     */
    public void setNameEN(String nameEN) {
        this.nameEN = nameEN;
    }

    /**
     *
     * @return
     * The year
     */
    public String getYear() {
        return year;
    }

    /**
     *
     * @param year
     * The year
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     *
     * @return
     * The is3D
     */
    public Integer getIs3D() {
        return is3D;
    }

    /**
     *
     * @param is3D
     * The is3D
     */
    public void setIs3D(Integer is3D) {
        this.is3D = is3D;
    }

    /**
     *
     * @return
     * The rating
     */
    public String getRating() {
        return rating;
    }

    /**
     *
     * @param rating
     * The rating
     */
    public void setRating(String rating) {
        this.rating = rating;
    }

    /**
     *
     * @return
     * The posterURL
     */
    public String getPosterURL() {
        return posterURL;
    }

    /**
     *
     * @param posterURL
     * The posterURL
     */
    public void setPosterURL(String posterURL) {
        this.posterURL = posterURL;
    }

    /**
     *
     * @return
     * The filmLength
     */
    public String getFilmLength() {
        return filmLength;
    }

    /**
     *
     * @param filmLength
     * The filmLength
     */
    public void setFilmLength(String filmLength) {
        this.filmLength = filmLength;
    }

    /**
     *
     * @return
     * The country
     */
    public String getCountry() {
        return country;
    }

    /**
     *
     * @param country
     * The country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     *
     * @return
     * The genre
     */
    public String getGenre() {
        return genre;
    }

    /**
     *
     * @param genre
     * The genre
     */
    public void setGenre(String genre) {
        this.genre = genre;
    }

    /**
     *
     * @return
     * The videoURL
     */
    public VideoURL getVideoURL() {
        return videoURL;
    }

    /**
     *
     * @param videoURL
     * The videoURL
     */
    public void setVideoURL(VideoURL videoURL) {
        this.videoURL = videoURL;
    }

    /**
     *
     * @return
     * The items
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     *
     * @param items
     * The items
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }

    /**
     *
     * @return
     * The date
     */
    public String getDate() {
        return date;
    }

    /**
     *
     * @param date
     * The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     *
     * @return
     * The imdbID
     */
    public String getImdbID() {
        return imdbID;
    }

    /**
     *
     * @param imdbID
     * The imdbID
     */
    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }


    public static class VideoURL implements Serializable{

        @SerializedName("hd")
        @Expose
        private String hd;
        @SerializedName("sd")
        @Expose
        private String sd;
        @SerializedName("low")
        @Expose
        private String low;

        /**
         *
         * @return
         * The hd
         */
        public String getHd() {
            return hd;
        }

        /**
         *
         * @param hd
         * The hd
         */
        public void setHd(String hd) {
            this.hd = hd;
        }

        /**
         *
         * @return
         * The sd
         */
        public String getSd() {
            return sd;
        }

        /**
         *
         * @param sd
         * The sd
         */
        public void setSd(String sd) {
            this.sd = sd;
        }

        /**
         *
         * @return
         * The low
         */
        public String getLow() {
            return low;
        }

        /**
         *
         * @param low
         * The low
         */
        public void setLow(String low) {
            this.low = low;
        }

    }



    public static class Seance3D implements Serializable{

        @SerializedName("time")
        @Expose
        private String time;

        /**
         *
         * @return
         * The time
         */
        public String getTime() {
            return time;
        }

        /**
         *
         * @param time
         * The time
         */
        public void setTime(String time) {
            this.time = time;
        }

    }


    public static class Seance implements Serializable{

        @SerializedName("time")
        @Expose
        private String time;

        /**
         *
         * @return
         * The time
         */
        public String getTime() {
            return time;
        }

        /**
         *
         * @param time
         * The time
         */
        public void setTime(String time) {
            this.time = time;
        }

    }



    public static class Item implements Serializable{

        @SerializedName("cinemaID")
        @Expose
        private Integer cinemaID;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("lon")
        @Expose
        private String lon;
        @SerializedName("lat")
        @Expose
        private String lat;
        @SerializedName("cinemaName")
        @Expose
        private String cinemaName;
        @SerializedName("seance3D")
        @Expose
        private List<Seance3D> seance3D = new ArrayList<Seance3D>();
        @SerializedName("seance")
        @Expose
        private List<Seance> seance = new ArrayList<Seance>();

        /**
         *
         * @return
         * The cinemaID
         */
        public Integer getCinemaID() {
            return cinemaID;
        }

        /**
         *
         * @param cinemaID
         * The cinemaID
         */
        public void setCinemaID(Integer cinemaID) {
            this.cinemaID = cinemaID;
        }

        /**
         *
         * @return
         * The address
         */
        public String getAddress() {
            return address;
        }

        /**
         *
         * @param address
         * The address
         */
        public void setAddress(String address) {
            this.address = address;
        }

        /**
         *
         * @return
         * The lon
         */
        public String getLon() {
            return lon;
        }

        /**
         *
         * @param lon
         * The lon
         */
        public void setLon(String lon) {
            this.lon = lon;
        }

        /**
         *
         * @return
         * The lat
         */
        public String getLat() {
            return lat;
        }

        /**
         *
         * @param lat
         * The lat
         */
        public void setLat(String lat) {
            this.lat = lat;
        }

        /**
         *
         * @return
         * The cinemaName
         */
        public String getCinemaName() {
            return cinemaName;
        }

        /**
         *
         * @param cinemaName
         * The cinemaName
         */
        public void setCinemaName(String cinemaName) {
            this.cinemaName = cinemaName;
        }

        /**
         *
         * @return
         * The seance3D
         */
        public List<Seance3D> getSeance3D() {
            return seance3D;
        }

        /**
         *
         * @param seance3D
         * The seance3D
         */
        public void setSeance3D(List<Seance3D> seance3D) {
            this.seance3D = seance3D;
        }

        /**
         *
         * @return
         * The seance
         */
        public List<Seance> getSeance() {
            return seance;
        }

        /**
         *
         * @param seance
         * The seance
         */
        public void setSeance(List<Seance> seance) {
            this.seance = seance;
        }

    }

}