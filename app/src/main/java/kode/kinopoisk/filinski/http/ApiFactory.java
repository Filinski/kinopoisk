package kode.kinopoisk.filinski.http;

import kode.kinopoisk.filinski.BuildConfig;
import kode.kinopoisk.filinski.http.pojo.CityPojo;
import kode.kinopoisk.filinski.http.pojo.FilmPojo;
import kode.kinopoisk.filinski.http.pojo.Genres;
import kode.kinopoisk.filinski.http.pojo.SeancesPojo;
import kode.kinopoisk.filinski.http.pojo.TodayFilmsPojo;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Fill on 25.09.2016.
 */

public class ApiFactory {

    public static final String APIURL = BuildConfig.BASE_URL;

    public static final String GET_TODAY_FILMS = "/getTodayFilms";
    public static final String GET_GENRES = "/getGenres";
    public static final String GET_FILM = "/getFilm";
    public static final String GET_SEANCES = "getSeance";

    public static final String GET_CITY_LIST = "/getCityList";

    public interface Kinopoisk{

        @GET(GET_CITY_LIST)
        Observable<CityPojo> getCity(@Query("countryID") int countryID);

        @GET(GET_TODAY_FILMS)
        Observable<TodayFilmsPojo> getTodayFilms(@Query("cityID") long cityID, @Query("genreID") String genre);

        @GET(GET_TODAY_FILMS)
        Observable<TodayFilmsPojo> getTodayFilms(@Query("cityID") long cityID);

        @GET(GET_GENRES)
        Observable<Genres> getGenres();

        @GET(GET_FILM)
        Observable<FilmPojo> getFilm(@Query("filmID") long filmID);

        @GET(GET_SEANCES)
        Observable<SeancesPojo> getSeances(@Query("filmID") long filmID, @Query("cityID") long cityID);




    }




}
