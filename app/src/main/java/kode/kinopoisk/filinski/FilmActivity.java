package kode.kinopoisk.filinski;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import kode.kinopoisk.filinski.http.Gateway;
import kode.kinopoisk.filinski.http.pojo.FilmPojo;
import kode.kinopoisk.filinski.utils.L;
import kode.kinopoisk.filinski.utils.SpHelper;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Fill on 28.09.2016.
 */

public class FilmActivity extends AppCompatActivity {


    @BindView(R.id.collapse_img)
    ImageView poster_img;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout  collapsing_toolbar;

    @BindView(R.id.container)
    LinearLayout container;

    SpHelper spHelper;

    Gateway gateway;
    String filmID;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film);
        ButterKnife.bind(this);

        gateway = Gateway.getInstance();

        Intent intent = getIntent();
        filmID = intent.getExtras().getString("filmID");

        setSupportActionBar(toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();

        getFilm(filmID);


    }

    private void getFilm(String filmID){

        gateway.film(Long.valueOf(filmID))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(abstractPojo -> {

                    FilmPojo pojo = (FilmPojo)abstractPojo;
                    String posterUrl = "https://st.kp.yandex.net/images/"+pojo.getPosterURL();
                    posterUrl = posterUrl.replace("iphone_", "iphone360_"); // глюк API
                    Picasso.with(FilmActivity.this).load(posterUrl).into(poster_img);

                    collapsing_toolbar.setTitle(pojo.getNameRU());

                });

    }

    //
//    год
//    2016
//    страна
//            Ирландия, Великобритания, Франция, США
//    слоган	«Её ждут большие перемены»
//    режиссер	Шэрон Магуайр
//    сценарий	Хелен Филдинг, Дэн Мазер, Эмма Томпсон
//    продюсер	Тим Беван, Лиза Чейсин, Эрик Феллнер, ...
//    оператор	Эндрю Данн
//    композитор	Крэйг Армстронг
//    художник	Джон Пол Келли, Дэвид Хиндл, Джонатан Хоулдинг, ...
//    монтаж	Мелани Оливер
//    жанр	мелодрама, комедия, ... слова
//            бюджет
//    $35 000 000
//    сборы в США
//    $16 984 910
//    сборы в мире
//    + $67 415 358 = $84 400 268 сборы
//    сборы в России
//    $2 593 359
//    зрители
//    Россия  637.2 тыс.
//            премьера (мир)
//            14 сентября 2016, ...
//    премьера (РФ)
//    15 сентября 2016, «UPI»
//    возраст
//    рейтинг MPAA	рейтинг R
//    время	123 мин. / 02:03
}
